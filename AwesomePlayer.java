import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * AwesomePlayer plays the poker hands game awesomely!
 */
public class AwesomePlayer implements PokerSquaresPlayer {

    private final int SIZE = 5; // number of rows/columns in square grid
    private final int NUM_POS = SIZE * SIZE; // number of positions in square grid
    private final int NUM_CARDS = Card.NUM_CARDS; // number of cards in deck
    private Card[][] grid = new Card[SIZE][SIZE]; // grid with Card objects or null (for empty positions)
    private PokerSquaresPointSystem system;


    /* (non-Javadoc)
     * @see PokerSquaresPlayer#setPointSystem(PokerSquaresPointSystem, long)
     */
    @Override
    public void setPointSystem(PokerSquaresPointSystem system, long millis) {
        this.system = system;
    }

    ArrayList<Card> realDeck;
    ArrayList<ArrayList<Card>> realRows;

    /* (non-Javadoc)
     * @see PokerSquaresPlayer#init()
     */
    @Override
    public void init() {
        // clear grid
        for (int row = 0; row < SIZE; row++)
            for (int col = 0; col < SIZE; col++)
                grid[row][col] = null;
        realDeck = new ArrayList<Card>(Arrays.asList(Card.getAllCards()));
        realRows = new ArrayList<ArrayList<Card>>();
        for (int i = 0; i < 10; i++)
            realRows.add(new ArrayList<>());
    }

    /* (non-Javadoc)
     * @see PokerSquaresPlayer#getPlay(Card, long)
     */
    @Override
    public int[] getPlay(Card card, long millisRemaining) {
        int cardrow = 0;
        int cardcol = 0;

        int cardrank = card.getRank();
        int cardsuit = card.getSuit();

        ArrayList<Double> scores = new ArrayList<>();
        realDeck.remove(card);

        for (ArrayList<Card> row : realRows) {
            double scoreDiff = -ProbabilityCalculator.getRowUtility(system, row, realDeck);
            row.add(card);
            scoreDiff += ProbabilityCalculator.getRowUtility(system, row, realDeck);
            row.remove(card);
            scores.add(scoreDiff);
        }

        double maxScore = -1000000;

        for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++) {
                if (grid[i][j] == null && scores.get(i) + scores.get(5 + j) > maxScore) {
                    maxScore = scores.get(i) + scores.get(5 + j);
                    cardrow = i;
                    cardcol = j;
//                    System.err.println("[" + cardrow + ", " + cardcol + "] => " + (scores.get(i) + scores.get(5 + j)));
                }
//                System.err.println("MAXScore: " + maxScore + " s " + (scores.get(i) + scores.get(5 + j)) + " " + (grid[i][j] == null));
            }

        grid[cardrow][cardcol] = card;
        realRows.get(cardrow).add(card);
        realRows.get(cardcol + 5).add(card);

        return new int[]{cardrow, cardcol};
    }

    /* (non-Javadoc)
     * @see PokerSquaresPlayer#getName()
     */
    @Override
    public String getName() {
        return "AwesomePlayer";
    }


    /**
     * Demonstrate AwesomePlayer play with British point system.
     *
     * @param args (not used)
     */
    public static void main(String[] args) {
        PokerSquaresPointSystem system = PokerSquaresPointSystem.getBritishPointSystem();
//        System.out.println(system);
        new PokerSquares(new AwesomePlayer(), system).play(); // play a single game
    }

}

/**
 * A bitmap of cards, that helps finding hands in a set of cards.
 */
class CardsBitMap {
    private int[] map = new int[4];

    void addCards(ArrayList<Card> cards) {
        for (Card c : cards)
            map[c.getSuit()] |= 1 << c.getRank();
    }

    public String toString() {
        return Card.getSuitNames()[0] + " => " + Integer.toBinaryString(map[0]) + "\n" +
                Card.getSuitNames()[1] + " => " + Integer.toBinaryString(map[1]) + "\n" +
                Card.getSuitNames()[2] + " => " + Integer.toBinaryString(map[2]) + "\n" +
                Card.getSuitNames()[3] + " => " + Integer.toBinaryString(map[3]);
    }

    int getSuitBits(int suit) {
        return map[suit];
    }

    boolean hasStraightFlush(int startAt, int suit, ArrayList<Card> row) {
        for (int i = startAt; i < startAt + 5; i++) {
            if ((map[suit] & (1 << (i % 13))) == 0)
                if (!row.contains(Card.getCard(suit * 13 + (i % 13))))
                    return false;
        }
        return true;
    }

    long getStraightHands(int startAt, ArrayList<Card> row, int spotsLeft) {
        long result = 1;
        int used_spots = 0;
        for (int i = startAt; i < startAt + 5; i++) {
            long c = 0;
            boolean used_new_card = false;
            for (int suit = 0; suit < 4; suit++) {
                if (row.contains(Card.getCard(suit * 13 + (i % 13)))) {
                    c = 1;
                    used_new_card = false;
                    break;
                }
                else if ((map[suit] & (1 << (i % 13))) != 0) {
                    c++;
                    used_new_card = true;
                }
            }
            if (used_new_card)
                used_spots++;
            result *= c;
        }
        if (used_spots > spotsLeft)
            return 0;
        return result;
    }

    int getFourOfAKindsBits() {
        return map[0] & map[1] & map[2] & map[3];
    }

    int findCardsWithRank(int rank) {
        int result = 0;
        for (int i = 0; i < 4; i++)
            if ((map[i] & (1 << rank)) != 0)
                result++;
        return result;
    }

    int getAllPairsHands(int exclude) {
        int result = 0;
        for (int i = 0; i < 4; i++)
            for (int j = i + 1; j < 4; j++) {
                int pairs = map[i] & map[j];
                int excludedPair = pairs & exclude;
                result += popcount(pairs) - popcount(excludedPair);
            }
        return result;
    }

    int getThreeOfAKindBits() {
        return (map[0] & map[1] & map[2]) |
                (map[0] & map[1] & map[3]) |
                (map[0] & map[2] & map[3]) |
                (map[1] & map[2] & map[3]);
    }

    int getAllRankBits() {
        return map[0] | map[1] | map[2] | map[3];
    }

    static ArrayList<Integer> getRanksList(int i) {
        ArrayList<Integer> result = new ArrayList<>();
        int rank = 0;
        while (i != 0) {
            if (i % 2 == 1)
                result.add(rank);
            i /= 2;
            rank++;
        }
        return result;
    }

    static int popcount(int i) {
        return getRanksList(i).size();
    }
}

class ProbabilityCalculator {
    private static long binomial(int n, int k) {
        if (k > n)
            return 0;

        if (k > n - k)
            k = n - k;

        long b = 1;
        for (int i = 1, m = n; i <= k; i++, m--)
            b = b * m / i;
        return b;
    }

    static long getAllHands(ArrayList<Card> row, ArrayList<Card> cards) {
        return binomial(cards.size(), 5 - row.size());
    }

    static long getStraightFlushHands(ArrayList<Card> row, ArrayList<Card> cards) {

        // TODO: In straight, ace cannot bo both 1 and higher than king

        int maxRank = -1;
        int minRank = -1;
        int suit = -1;

        CardsBitMap map = new CardsBitMap();
        map.addCards(cards);

        for (Card c1 : row) {
            boolean isMax = true;
            boolean isMin = true;
            for (Card c2 : row) {
                int rankDiff = c1.getRank() - c2.getRank();
                if (c1.getSuit() != c2.getSuit())
                    return 0;
                if (Math.floorMod(rankDiff, 13) > 4)
                    isMax = false;
                if (Math.floorMod(-rankDiff, 13) > 4)
                    isMin = false;
            }
            if (isMax)
                maxRank = c1.getRank();
            if (isMin)
                minRank = c1.getRank();
            suit = c1.getSuit();
        }

        int totalStraightFlushes = 0;

        if (suit != -1) {
            if (maxRank == -1 || minRank == -1)
                return 0;

            if (maxRank < minRank && maxRank != 0)
                return 0;

            int i = Math.floorMod(maxRank - 5, 13);
            while (i != minRank) {
                i = Math.floorMod(i + 1, 13);

                if (map.hasStraightFlush(i, suit, row))
                    totalStraightFlushes++;
            }
        } else {
            for (suit = 0; suit < 4; suit++)
                for (int i = 0; i < 10; i++)
                    if (map.hasStraightFlush(i, suit, row))
                        totalStraightFlushes++;
        }
        return totalStraightFlushes;
    }

    static long getFourOfAKindHands(ArrayList<Card> row, ArrayList<Card> cards) {
        CardsBitMap map = new CardsBitMap();
        map.addCards(cards);

        HashMap<Integer, Integer> validRanks = new HashMap<Integer, Integer>();

        int candidateRank = -1;
        for (Card c : row) {
            if (validRanks.containsKey(c.getRank())) {
                validRanks.replace(c.getRank(), validRanks.get(c.getRank()) + 1);
                if (candidateRank != -1)
                    // Two ranks with more than one card
                    return 0;
                candidateRank = c.getRank();
                continue;
            }
            validRanks.put(c.getRank(), 1);
        }

        long total = 0;

        if (validRanks.size() > 2)
            return 0;

        if (validRanks.size() == 2) {
            if (candidateRank != -1) {
                if (map.findCardsWithRank(candidateRank) + validRanks.get(candidateRank) == 4)
                    return 1;
                return 0;
            }
            for (Map.Entry<Integer, Integer> entry : validRanks.entrySet()) {
                if (map.findCardsWithRank(entry.getKey()) + entry.getValue() == 5)
                    return 1;
            }
            return 0;
        } else if (validRanks.size() == 1) {
            candidateRank = validRanks.keySet().toArray(new Integer[]{})[0];
            int cardsWithRank = map.findCardsWithRank(candidateRank);
            if (cardsWithRank + validRanks.get(candidateRank) == 4)
                total += cards.size() - cardsWithRank; // The other card can be any of the cards
            if (validRanks.get(candidateRank) == 1)
                total += CardsBitMap.popcount(map.getFourOfAKindsBits());
        } else {
            total = CardsBitMap.popcount(map.getFourOfAKindsBits()) * (cards.size() - 4);
        }

        return total;
    }

    static long getFullHouseHands(ArrayList<Card> row, ArrayList<Card> cards) {
        CardsBitMap map = new CardsBitMap();
        map.addCards(cards);

        HashMap<Integer, Integer> validRanks = new HashMap<Integer, Integer>();

        for (Card c : row) {
            if (validRanks.containsKey(c.getRank())) {
                validRanks.replace(c.getRank(), validRanks.get(c.getRank()) + 1);
                continue;
            }
            if (validRanks.size() >= 2)
                return 0;
            validRanks.put(c.getRank(), 1);
        }

        for (Map.Entry<Integer, Integer> entry : validRanks.entrySet()) {
            if (entry.getValue() > 3)
                return 0;
            if (entry.getValue() == 3)
                return getFullHouseHands2(row, cards, entry.getKey());
        }

        if (validRanks.size() == 2) {
            AtomicLong result = new AtomicLong();
            validRanks.forEach((rank, count) -> {
                int cardsWithRank = map.findCardsWithRank(rank);
                int threes = (int) binomial(cardsWithRank, 3 - count);
                result.addAndGet(threes * getFullHouseHands2(row, cards, rank));
            });
            return result.get();
        } else if (validRanks.size() == 1) {
            AtomicLong result = new AtomicLong();
            validRanks.forEach((rank, count) -> {
                int cardsWithRank = map.findCardsWithRank(rank);
                int threes = (int) binomial(cardsWithRank, 3 - count);
                result.addAndGet(threes * getFullHouseHands2(row, cards, rank));
            });

            ArrayList<Integer> otherPossibleThrees = CardsBitMap.getRanksList(map.getThreeOfAKindBits());
            for (Integer threeRank : otherPossibleThrees) {
                if (validRanks.containsKey(threeRank))
                    continue;
                int cardWithThreeRank = map.findCardsWithRank(threeRank);
                int threes = cardWithThreeRank == 4 ? 4 : 1;
                result.addAndGet(threes * getFullHouseHands2(row, cards, threeRank));
            }
            return result.get();
        } else {
            long result = 0;
            ArrayList<Integer> otherPossibleThrees = CardsBitMap.getRanksList(map.getThreeOfAKindBits());
            for (Integer threeRank : otherPossibleThrees) {
                int cardWithThreeRank = map.findCardsWithRank(threeRank);
                int threes = cardWithThreeRank == 4 ? 4 : 1;
                result += threes * getFullHouseHands2(row, cards, threeRank);
            }
            return result;
        }
    }

    static long getFullHouseHands2(ArrayList<Card> row, ArrayList<Card> cards, int threeRank) {
        CardsBitMap map = new CardsBitMap();
        map.addCards(cards);

        int twoRank = -1;
        for (Card c : row) {
            if (c.getRank() == threeRank)
                continue;
            if (twoRank != -1)
                if (c.getRank() != twoRank) {
//                    System.out.println("THIS SHOULD NEVER HAPPEN!!!!");
                    return 0;
                } else
                    return 1;
            twoRank = c.getRank();
        }

        if (twoRank != -1)
            return map.findCardsWithRank(twoRank);
        return map.getAllPairsHands(1 << threeRank);
    }

    static long getFlushHands(ArrayList<Card> row, ArrayList<Card> cards) {
        CardsBitMap map = new CardsBitMap();
        map.addCards(cards);

        int suit = -1;
        for (Card c : row) {
            if (suit != -1 && c.getSuit() != suit)
                return 0;
            suit = c.getSuit();
        }

        if (suit != -1) {
            int rowSize = row.size();

            int otherCards = CardsBitMap.popcount(map.getSuitBits(suit));
            return binomial(otherCards, 5 - rowSize);
        }
        long result = 0;
        for (suit = 0; suit < 4; suit++) {
            int otherCards = CardsBitMap.popcount(map.getSuitBits(suit));
            result += binomial(otherCards, 5);
        }
        return result;
    }

    static long getStraightHands(ArrayList<Card> row, ArrayList<Card> cards) {
        int maxRank = -1;
        int minRank = -1;
        int spotsLeft = 5 - row.size();

        CardsBitMap map = new CardsBitMap();
        map.addCards(cards);

        for (Card c1 : row) {
            boolean isMax = true;
            boolean isMin = true;
            for (Card c2 : row) {
                int rankDiff = c1.getRank() - c2.getRank();
                if (Math.floorMod(rankDiff, 13) > 4)
                    isMax = false;
                if (Math.floorMod(-rankDiff, 13) > 4)
                    isMin = false;
            }
            if (isMax)
                maxRank = c1.getRank();
            if (isMin)
                minRank = c1.getRank();
        }

        int totalStraights = 0;

        if (row.size() != 0) {
            if (maxRank == -1 || minRank == -1)
                return 0;

            if (maxRank < minRank && maxRank != 0)
                return 0;

            int i = Math.floorMod(maxRank - 5, 13);
            while (i != minRank) {
                i = Math.floorMod(i + 1, 13);
                totalStraights += map.getStraightHands(i, row, spotsLeft);
            }
        } else {
            for (int i = 0; i < 10; i++) {
                totalStraights += map.getStraightHands(i, row, spotsLeft);
            }
        }
        return totalStraights; // TODO: minus straight flushes
    }

    static long getThreeOfAKindHands(ArrayList<Card> row, ArrayList<Card> cards) {
        CardsBitMap map = new CardsBitMap();
        map.addCards(cards);

        int cardsToFill = 5 - row.size(); // Number of empty spaces for cards in the row

        HashMap<Integer, Integer> validRanks = new HashMap<Integer, Integer>(); // Map of cards in the row to their count

        for (Card c : row) {
            if (validRanks.containsKey(c.getRank())) {
                validRanks.replace(c.getRank(), validRanks.get(c.getRank()) + 1);
                continue;
            }
            validRanks.put(c.getRank(), 1);
        }

        AtomicLong total = new AtomicLong();
        for (Map.Entry<Integer, Integer> entry : validRanks.entrySet()) {
            int cardsToPick = 3 - entry.getValue(); // Minimum number of cards to pick from deck, to form a three of a kind
            if (cardsToFill < cardsToPick)
                continue; // Can't create a three of a kind hand, because there is not enough room for it in hand!

            if (entry.getValue() >= 3)
                return binomial(cards.size(), cardsToFill); // Already have the hand, gotta fill the row

            int cardsWithRank = map.findCardsWithRank(entry.getKey()); // Number of cards in deck with entry's rank
            if (cardsWithRank == cardsToPick + 1) // We have more cards than we need! (We need 3 total, but there are 4 cards with this rank in row + deck)
            {
                // First add all ToaKs => ways to pick cards with rank to create ToaK * ways to fill the rest of row
                total.addAndGet(binomial(cardsWithRank, cardsToPick) * binomial(cards.size() - cardsWithRank, cardsToFill - cardsToPick));
                if (cardsToFill > cardsToPick) // If we have enough room for all four of cards
                    // Ways to fill the rest of the row
                    total.addAndGet(binomial(cards.size() - cardsWithRank, cardsToFill - cardsToPick - 1));
            } else if (cardsWithRank == cardsToPick) // We have what just what we need!
                // Ways to fill the rest of the row
                total.addAndGet(binomial(cards.size() - cardsWithRank, cardsToFill - cardsToPick));
        }

        if (cardsToFill >= 3) { // Should have enough room to get all the cards of this hand out of the deck!
            CardsBitMap.getRanksList(map.getThreeOfAKindBits()).forEach(rank -> { // For each of the possible ranks
                if (map.findCardsWithRank(rank) == 4) { // Four cards in the deck with this rank
                    // Ways to pick three cards with rank to create ToaK (4) * ways to fill the rest of row
                    total.addAndGet(4 * binomial(cards.size() - 4, cardsToFill - 3));
                    if (cardsToFill >= 4) // We have room for a four of a kind
                        // Ways to fill the rest of row
                        total.addAndGet(binomial(cards.size() - 4, cardsToFill - 4));
                } else {
                    // Ways to fill the rest of row
                    total.addAndGet(binomial(cards.size() - 3, cardsToFill - 3));
                }
            });
        }


        return total.get();
    }

    static long getTwoPairHands(ArrayList<Card> row, ArrayList<Card> cards) {
        CardsBitMap map = new CardsBitMap();
        map.addCards(cards);

        int cardsToFill = 5 - row.size(); // Number of empty spaces for cards in the row

        HashMap<Integer, Integer> validRanks = new HashMap<Integer, Integer>(); // Map of cards in the row to their count

        for (Card c : row) {
            if (validRanks.containsKey(c.getRank())) {
                validRanks.replace(c.getRank(), validRanks.get(c.getRank()) + 1);
                continue;
            }
            validRanks.put(c.getRank(), 1);
        }

        HashMap<Integer, Long> combs = new HashMap<>();
        HashMap<Integer, Integer> space = new HashMap<>();
        int pairsNeeded = 2;
        for (int i = 0; i < 13; i++) {
            int cardsToHave = 2 - validRanks.getOrDefault(i, 0);
            if (cardsToHave <= 0)
                pairsNeeded--;
            else {
                if (cardsToHave > cardsToFill)
                    continue;
                if (map.findCardsWithRank(i) >= cardsToHave) {
                    if (cardsToHave == 1) {
                        combs.put(i, (long) map.findCardsWithRank(i));
                    } else if (cardsToHave == 2) {
                        combs.put(i, binomial(map.findCardsWithRank(i), cardsToHave));
                    }
                    space.put(i, cardsToHave);
                }
            }
        }

        if (pairsNeeded == 0) {
            if (cardsToFill == 1) {
                return cards.size();
            }
            return 1;
        }


        long overallScore = 0;
        Integer[] combKeySet = combs.keySet().toArray(new Integer[0]);
        for (int i = 0; i < combKeySet.length; i++) {
            int rank = combKeySet[i];
            if (cardsToFill < space.get(combKeySet[i]))
                continue;
            if (pairsNeeded == 1) {
                overallScore += binomial(cards.size() - map.findCardsWithRank(rank), 5 - (row.size() + space.get(combKeySet[i])))
                        * combs.get(rank);
                continue;
            }
            for (int j = i + 1; j < combKeySet.length; j++) {
                if (cardsToFill < space.get(combKeySet[i]) + space.get(combKeySet[j]))
                    continue;
                overallScore += combs.get(rank) * combs.get(combKeySet[j]) * binomial(cards.size() - map.findCardsWithRank(rank) - map.findCardsWithRank(combKeySet[j]),
                        5 - (row.size() + space.get(combKeySet[i]) + space.get(combKeySet[j])));
            }
        }

        return overallScore;
    }

    static long getOnePairHands(ArrayList<Card> row, ArrayList<Card> cards) {
        CardsBitMap map = new CardsBitMap();
        map.addCards(cards);

        int cardsToFill = 5 - row.size(); // Number of empty spaces for cards in the row

        HashMap<Integer, Integer> validRanks = new HashMap<Integer, Integer>(); // Map of cards in the row to their count

        for (Card c : row) {
            if (validRanks.containsKey(c.getRank())) {
                validRanks.replace(c.getRank(), validRanks.get(c.getRank()) + 1);
                continue;
            }
            validRanks.put(c.getRank(), 1);
        }

        long total = 0;

        for (Map.Entry<Integer, Integer> entry : validRanks.entrySet())
            if (entry.getValue() >= 2)
                return binomial(cards.size(), 5 - row.size());

        class State {
            int rowSpace;
            int coef;
            int remainingCards;
        }
        int usedUpCards = 0;
        ArrayList<State> states = new ArrayList<>();
        states.add(new State() {{
            rowSpace = cardsToFill;
            coef = 1;
            remainingCards = cards.size();
        }});

        for (int rank = 0; rank < 13; rank++) {
            int cardsWithRank = map.findCardsWithRank(rank);

            int cardsNeeded = 2;
            if (validRanks.containsKey(rank)) {
                cardsNeeded -= validRanks.get(rank);
            }

            if (cardsNeeded > cardsWithRank)
                continue;

            ArrayList<State> newStates = new ArrayList<>();
            for (State s : states) {
                if (cardsNeeded == 1) {
                    newStates.add(new State() {{
                        rowSpace = s.rowSpace;
                        coef = s.coef;
                        remainingCards = s.remainingCards - cardsWithRank;
                    }});
                }

                if (cardsNeeded == 2) {
                    newStates.add(new State() {{
                        rowSpace = s.rowSpace;
                        coef = s.coef;
                        remainingCards = s.remainingCards - cardsWithRank;
                    }});
                    if (s.rowSpace > 1)
                        newStates.add(new State() {{
                            rowSpace = s.rowSpace - 1;
                            coef = s.coef * cardsWithRank;
                            remainingCards = s.remainingCards - cardsWithRank;
                        }});
                }

                int cardToPut = cardsNeeded;

                while (cardToPut <= cardsWithRank) {
                    if (cardToPut > s.rowSpace) // Should only happen if no cards is row, and only one space left in row
                        break;

                    total += s.coef * binomial(s.remainingCards - cardsWithRank, s.rowSpace - cardToPut) *
                            binomial(cardsWithRank, cardToPut);
                    cardToPut++;
                }
            }
            states = newStates;
        }

        return total;
    }

    static HashMap<Integer, HashMap<Integer, Double>> utilitiesCache = new HashMap<>();
    static int called = 0;

    static void clearCache() {
        utilitiesCache = new HashMap<>();
    }

    static double getRowUtility(PokerSquaresPointSystem system, ArrayList<Card> row, ArrayList<Card> cards) {
        if (utilitiesCache.containsKey(row.hashCode()) && utilitiesCache.get(row.hashCode()).containsKey(cards.hashCode()))
            return utilitiesCache.get(row.hashCode()).get(cards.hashCode());
        called++;

        long allHands = getAllHands(row, cards);
        long straightFlush = getStraightFlushHands(row, cards);
        double straightFlushProb = (double) straightFlush / allHands;
        long fourOfAKind = getFourOfAKindHands(row, cards);
        double fourOfAKindProb = (double) fourOfAKind / allHands;
        long fullHouse = getFullHouseHands(row, cards);
        double fullHouseProb = (double) fullHouse / allHands;
        long flush = getFlushHands(row, cards);
        double flushProb = (double) flush / allHands;
        long straight = getStraightHands(row, cards);
        double straightProb = (double) straight / allHands;
        long threeOfAKind = getThreeOfAKindHands(row, cards);
        double threeOfAKindProb = (double) threeOfAKind / allHands;
        long twoPairs = getTwoPairHands(row, cards);
        double twoPairsProb = (double) twoPairs / allHands;
        long onePairs = getOnePairHands(row, cards);
        double onePairsProb = (double) onePairs / allHands;

//        System.out.println("All Hands: " + allHands);

        double total_prob = 0;

        double score = 0;

        score += (1 - total_prob) * straightFlushProb * system.getScoreTable()[8];
        total_prob += straightFlushProb * (1 - total_prob);
        //System.out.println("StraightFlush: " + straightFlush + "(P = " + straightFlushProb + ") " + score + " " + total_prob);
        score += (1 - total_prob) * fourOfAKindProb * system.getScoreTable()[7];
        total_prob += fourOfAKindProb * (1 - total_prob);
        //System.out.println("FourOfAKind: " + fourOfAKind + "(P = " + fourOfAKindProb + ") " + score + " " + total_prob);
        score += (1 - total_prob) * fullHouseProb * system.getScoreTable()[6];
        total_prob += fullHouseProb * (1 - total_prob);
//        System.out.println("FullHouse: " + fullHouse + "(P = " + fullHouseProb + ") " + score + " " + total_prob);
        score += (1 - total_prob) * flushProb * system.getScoreTable()[5];
        total_prob += flushProb * (1 - total_prob);
//        System.out.println("Flush: " + flush + "(P = " + flushProb + ") " + score + " " + total_prob);
        score += (1 - total_prob) * straightProb * system.getScoreTable()[4];
        total_prob += straightProb * (1 - total_prob);
//        System.out.println("Straight: " + straightProb + "(P = " + straightProb + ") " + score + " " + total_prob);
        score += (1 - total_prob) * threeOfAKindProb * system.getScoreTable()[3];
        total_prob += threeOfAKindProb * (1 - total_prob);
//        System.out.println("ThreeOfAKind: " + threeOfAKind + "(P = " + threeOfAKindProb + ") " + score + " " + total_prob);
        score += (1 - total_prob) * twoPairsProb * system.getScoreTable()[2];
        total_prob += twoPairsProb * (1 - total_prob);
//        System.out.println("TwoPairs: " + twoPairs + "(P = " + twoPairsProb + ") " + score + " " + total_prob);
        score += (1 - total_prob) * onePairsProb * system.getScoreTable()[1];
//        System.out.println("OnePairs: " + onePairs + "(P = " + onePairsProb + ") " + score + " " + total_prob);

//        System.out.println(row);
//        System.out.println("============================ Score: " + score);

        if (!utilitiesCache.containsKey(row.hashCode()))
            utilitiesCache.put(row.hashCode(), new HashMap<>());

        utilitiesCache.get(row.hashCode()).putIfAbsent(cards.hashCode(), score);
        return score;
    }

    static double getGridUtility(PokerSquaresPointSystem system, Card[][] grid) {
        ArrayList<Card> deck = new ArrayList<Card>(Arrays.asList(Card.getAllCards()));

        ArrayList<ArrayList<Card>> rows = new ArrayList<ArrayList<Card>>();

        for (int i = 0; i < 10; i++)
            rows.add(new ArrayList<Card>());

        for (int i = 0; i < 25; i++) {
            Card card = grid[i / 5][i % 5];
            if (card != null) {
                deck.remove(card);
                rows.get(i / 5).add(card);
                rows.get((i % 5) + 5).add(card);
            }
        }

        double utility = 0;
        for (int i = 0; i < 10; i++) {
            utility += getRowUtility(system, rows.get(i), deck);
        }
        return utility;
    }
}